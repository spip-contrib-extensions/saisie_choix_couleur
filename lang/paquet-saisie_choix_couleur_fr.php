<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-saisie_choix_couleur
// Langue: fr
// Date: 23-03-2021 13:26:04
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'saisie_choix_couleur_description' => 'Une saisie de formulaire pour proposer plusieurs choix de couleurs.',
	'saisie_choix_couleur_slogan' => 'Formidable : vous avez le choix de la couleur !',
);
?>